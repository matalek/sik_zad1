// Aleksander Matusiak
// Sieci komputerowe - zadanie 1.
// Program ileczekam

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <endian.h>

#include "err.h"

// zwraca czas zainicjowania z sukcesem połączenia tcp
uint64_t measure_tcp(char host[], char port[]) {
    int sock;
    struct addrinfo addr_hints;
    struct addrinfo *addr_result;

    int err;

    // 'converting' host/port in string to struct addrinfo
    memset(&addr_hints, 0, sizeof(struct addrinfo));
    addr_hints.ai_family = AF_INET; // IPv4
    addr_hints.ai_socktype = SOCK_STREAM;
    addr_hints.ai_protocol = IPPROTO_TCP;
    err = getaddrinfo(host, port, &addr_hints, &addr_result);
    if (err != 0)
        syserr("getaddrinfo: %s\n", gai_strerror(err));

    // initialize socket according to getaddrinfo results
    sock = socket(addr_result->ai_family, addr_result->ai_socktype, addr_result->ai_protocol);
    if (sock < 0)
        syserr("socket");

    // wyliczenie czasu stworzenia wiadomości
    struct timeval tv;
    gettimeofday(&tv,NULL);
    uint64_t start_time = 1000000 * tv.tv_sec + tv.tv_usec;

    // connect socket to the server
    if (connect(sock, addr_result->ai_addr, addr_result->ai_addrlen) < 0)
        syserr("connect");

    // wyliczenie czasu otrzymania odpowiedzi
    gettimeofday(&tv,NULL);
    uint64_t end_time = 1000000 * tv.tv_sec + tv.tv_usec;

    freeaddrinfo(addr_result);

    (void) close(sock); // socket would be closed anyway when the program ends

    return end_time - start_time;
}

// zwraca czas wysyłania pakietu udp i otrzymania odpowiedzi
uint64_t measure_udp(char host[], char port[]) {
    int sock;
    struct addrinfo addr_hints;
    struct addrinfo *addr_result;

    int flags, sflags;
    size_t len;
    ssize_t snd_len, rcv_len;
    struct sockaddr_in my_address;
    struct sockaddr_in srvr_address;
    socklen_t rcva_len;

    // 'converting' host/port in string to struct addrinfo
    (void) memset(&addr_hints, 0, sizeof(struct addrinfo));
    addr_hints.ai_family = AF_INET; // IPv4
    addr_hints.ai_socktype = SOCK_DGRAM;
    addr_hints.ai_protocol = IPPROTO_UDP;
    addr_hints.ai_flags = 0;
    addr_hints.ai_addrlen = 0;
    addr_hints.ai_addr = NULL;
    addr_hints.ai_canonname = NULL;
    addr_hints.ai_next = NULL;
    if (getaddrinfo(host, NULL, &addr_hints, &addr_result) != 0) {
        syserr("getaddrinfo");
    }

    my_address.sin_family = AF_INET; // IPv4
    my_address.sin_addr.s_addr =
        ((struct sockaddr_in*) (addr_result->ai_addr))->sin_addr.s_addr; // address IP
    my_address.sin_port = htons((uint16_t) atoi(port)); // port from the command line

    freeaddrinfo(addr_result);

    sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
        syserr("socket");

    // stworzenie wiadomości
    uint64_t *mesg = (uint64_t *) malloc (sizeof(uint64_t));
    if (mesg == NULL)
        syserr("malloc");

    // wyliczenie czasu stworzenia wiadomości i jej uzupełnienie
    struct timeval tv;
    gettimeofday(&tv,NULL);
    uint64_t start_time = 1000000 * tv.tv_sec + tv.tv_usec;
    *mesg = htobe64(start_time); // zamiana na bigendian
    len = sizeof(uint64_t);

    // wysłanie wiadomości
    sflags = 0;
    rcva_len = (socklen_t) sizeof(my_address);
    snd_len = sendto(sock, mesg, len, sflags,
        (struct sockaddr *) &my_address, rcva_len);
    if (snd_len != len) {
        syserr("partial / failed write");
    }

    // odebranie nowej wiadomości
    uint64_t new_mesg[2];
    len = sizeof(uint64_t[2]);

    flags = 0;
    rcva_len = (socklen_t) sizeof(srvr_address);
    rcv_len = recvfrom(sock, new_mesg, len, flags,
        (struct sockaddr *) &srvr_address, &rcva_len);
    if (rcv_len < 0) {
        syserr("read");
    }

    // wyliczenie czasu otrzymania odpowiedzi
    gettimeofday(&tv,NULL);
    uint64_t end_time = 1000000 * tv.tv_sec + tv.tv_usec;

    // wypisanie otrzymanych danych
    (void) fprintf(stderr, "%" PRIu64 " %" PRIu64 "\n", be64toh(new_mesg[0]), be64toh(new_mesg[1]));

    if (close(sock) == -1) { //very rare errors can occur here, but then
        syserr("close"); //it's healthy to do the check
    };

    return end_time - start_time;
}

int main(int argc, char *argv[]) {

    if (argc < 4)
        fatal("Usage: %s measurement_method host port\n", argv[0]);

    uint64_t time = 0;

    if (!strcmp(argv[1], "-t"))
        time = measure_tcp(argv[2], argv[3]);
    else if (!strcmp(argv[1], "-u"))
        time = measure_udp(argv[2], argv[3]);
    else
        fatal("Measurement method can be -t or -u\n");

    (void) printf("%" PRIu64 "\n", time);

    return 0;
}
