// Aleksander Matusiak
// Sieci komputerowe - zadanie 1.
// Program czekamnaudp

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <endian.h>

#include "err.h"

int main(int argc, char *argv[]) {

    if (argc < 2)
        fatal("Usage: %s port\n", argv[0]);

    int sock;
    int flags, sflags;
    struct sockaddr_in server_address;
    struct sockaddr_in client_address;

    socklen_t snda_len, rcva_len;
    ssize_t len, snd_len;

    sock = socket(AF_INET, SOCK_DGRAM, 0); // creating IPv4 UDP socket
    if (sock < 0)
        syserr("socket");
    // after socket() call; we should close(sock) on any execution path;
    // since all execution paths exit immediately, sock would be closed when program terminates

    server_address.sin_family = AF_INET; // IPv4
    server_address.sin_addr.s_addr = htonl(INADDR_ANY); // listening on all interfaces
    server_address.sin_port = htons((uint16_t) atoi(argv[1])); // port from the command line

    // bind the socket to a concrete address
    if (bind(sock, (struct sockaddr *) &server_address,
            (socklen_t) sizeof(server_address)) < 0)
        syserr("bind");

    snda_len = (socklen_t) sizeof(client_address);
    for (;;) {
        // odczytanie wiadomości
        uint64_t *mesg = (uint64_t *) malloc (sizeof(uint64_t));
        if (mesg == NULL)
            syserr("malloc");
        rcva_len = (socklen_t) sizeof(client_address);
        flags = 0; // we do not request anything special
        len = recvfrom(sock, mesg, sizeof(uint64_t), flags,
                (struct sockaddr *) &client_address, &rcva_len);
        if (len < 0)
            syserr("error on datagram from client socket");
        else {
            // wyliczanie aktualnego czasu
            struct timeval tv;
            gettimeofday(&tv,NULL);
            uint64_t time = 1000000 * tv.tv_sec + tv.tv_usec;

            // stworzenie nowej wiadomości
            uint64_t new_mesg[2] = {*mesg, htobe64(time)};
            len = sizeof(uint64_t[2]);

            // wysłanie nowej wiadomości
            sflags = 0;
            snd_len = sendto(sock, new_mesg, len, sflags,
                    (struct sockaddr *) &client_address, snda_len);

            if (snd_len != len)
                syserr("error on sending datagram to client socket");
        }
    }

    return 0;
}
