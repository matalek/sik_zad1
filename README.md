#Latency measurement#
## Project created as an assignment for *Computer networks* course ##

### Task ###
The purpose of this task is to measure the latency using different methods. You should:

* Create program `ileczekam`, which will be executed with three arguments: method of measurement, name of the target computer and port number to be used. We use two methods of measurement: using TCP (-t) or UDP (-u) protocol.

* Create program `czekamnaudp`, which is a server necessary to measure the latency using UDP protocol. We execute it by passing a port number as an argument.

### Grade ###
For this assignment I got 1.9 out of 2.0 points.